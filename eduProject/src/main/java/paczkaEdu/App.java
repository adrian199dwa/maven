package paczkaEdu;

public class App {

    public static void main( String[] args ) {
        System.out.println( "Hello World! I will check number 1." );
        App app = new App();
        System.out.println(app.kowalsky);
        System.out.println(app.isNumberEven(1));
    }
    
    String kowalsky = "Kowalski";
    
    public boolean isNumberEven(Integer number) {
    	return number % 2 == 0;
    }
}
