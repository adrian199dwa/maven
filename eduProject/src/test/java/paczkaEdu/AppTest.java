package paczkaEdu;

import org.junit.Test;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

public class AppTest {

	App app = new App();

	@Test
	public void isNumberEvenTest_even() {
		assertTrue(app.isNumberEven(6));
	}	
	
	@Test
	public void isNumberEvenTest_odd() {
		assertFalse(app.isNumberEven(1));
	}
	
	@Test
	public void kowalskyTest() {
		assertTrue(app.kowalsky.equals("Kowalski"));
	}
}
